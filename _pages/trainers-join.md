---
permalink: /trainers-join/
layout: splash
header:
  overlay_color: "#eb8599"
  overlay_image: "/assets/images/group-backs.jpg"
author_profile: false
title: "Join the trainer network"

feature_about:
  - image_path: /assets/images/icon-fruit.png
    title: "Trainer Network"
    excerpt: "The Digital Research Academy is first and foremost a trainer network.<br>
             
              **We focus on people rather than materials.** <br><br>

              To support a thriving trainer community our core team focuses on community management."
    btn_label: "Meet the community"
    btn_class: "btn--primary"
    url: "/trainers"
  - image_path: /assets/images/icon-ttt.png
    title: "Train-the-Trainer"
    excerpt: "Join our trainer network by completing our Train-the-Trainer program."
    btn_label: "Tell me more!"
    btn_class: "btn--primary"
    url: "/train-the-trainer"
---

{% include feature_row id="feature_about" type="left" %}


