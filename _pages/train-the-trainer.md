---
permalink: /train-the-trainer/
layout: splash
header:
  overlay_color: "#eb8599"
author_profile: false
title: "Train-the-Trainer program"
---


# Goals

This program serves three purposes: 

1. Give prospective trainers the time and space to learn about good, evidence-based teaching, 
2. Provide trainers with a community of like-minded peers, and
3. Ensure that all trainers of the Digital Research Academy know how to teach well.


<img src="/assets/images/icon-ttt.png" alt="The letters TTT for Train The Trainer." width="200"/>

# Parts of the training

The TTT consists of two parts: Didactics and Practicals.

## Part 1: Learning about Didactics

In this part we will learn how to be(come) good trainers.  We will do so by
teaching each other principles of teaching that are based on best practices. We
want to break the bad habit of teacher-centered teaching and work according to
evidence on how people learn well. Didactics-lesson preparation and teaching can be done
alone or in pairs.

- Each trainer (pair) prepares one lesson. We will choose lesson topics
  together as a group.
- After each session we make notes about our key take-aways.
- We all give feedback on the lesson content and how it was taught.



## Part 2: Practicals

In this part we will put our new knowledge into practice by teaching a course.
Courses may be taught alone or in pairs.
The format and type of course can be chosen freely. It just needs to feel 
like a course and not just a one hour session.

These courses will already be official Digital Research Academy courses and
course participants will pay a fee for participation (we can offer
stipends).  This way we can finance the trainer
training and learn how well the course (type) sells.

- Each trainer (pair) prepares a course. The topic and course format is up
  to the respective trainer (pair).
- At least two other trainers serve as peer reviewers and give feedback
  both during course development and after the course was run.
- Feedback needs to be implemented into the course (material) after the course
  was run.
- Course material is made available under a CC-BY license as part of the
  Digital Research Academy material collection.

## Checklist

To earn the Digital Research Academy Trainer badge, you need to be able to
check all the following boxes:

### Part 1:

- [ ] Participated in the Kick-Off
- [ ] Participated in the evidence-based training session
- [ ] Participated in at least 4 didactics sessions 
- [ ] Taught one of the didactics sessions (does not count towards the 4 above)

### Part 2:

- [ ] Served as reviewer of at least two courses of peers
- [ ] Prepared and held one course
- [ ] Worked in the feedback of reviewers into the course
- [ ] Published the course as part of the Digital Research Academy resource
  collection

## Next TTT

[Apply for the next TTT!](https://events.digital-research.academy/event/32/){: .btn .btn--primary .btn--large}
{: style="text-align: center;"}

