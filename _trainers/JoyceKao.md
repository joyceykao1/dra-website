---
title: Joyce Kao
excerpt: Open Science, Research Software Engineering
header:
   teaser: ../assets/images/trainer_images/JoyceKao.jpeg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/JoyceKao.jpeg
     image_alt: Joyce
---

### Topics:

Open Science, Research Software Engineering

### Background:

I have a PhD in Computational Biology and Bioinformatics and was the
Founder of Open Innovation in Life Sciences (a non-profit association to
promote OS in life science ECRs in Switzerland). I worked as a
full-stack web developer for a while setting up agile team app
development processes (Scrum). My current position as Senior Project
Manager allows me to practice traditional/agile project and program
management in EU grants. Soon to be PMP and PSM certified… :)

### Training style:

Interactive workshops, flipped classroom, learning by doing

### Additional info:

I am obsessed with mangoes of all kinds.

City: Aachen <br/> Country: Germany <br/> ☑ Happy to offer services
online <br/> ➢ I am happy to travel (No)
