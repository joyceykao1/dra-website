# Welcome to the Digital Research Academy website project!
:tada: :wave: :wink:

## Making edits to the website


We work accoring to a [forking workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html):

1. Fork the project.
2. Edit the files you would like to change. Some info on the most likely edits you will make:
	* Content files are in the `_pages` folder. 
	* The skin (colors, fonts, etc) can be changed by editing `_sass/minimal-mistakes/skins/_dra.scss`
	* Images are placed in `assets/images`
3. Optional: install [jekyll](https://jekyllrb.com/docs/) and all dependencies for the website (`bundle install`) and check what the website looks like
```
bundle exec jekyll serve
```
4. Create a [merge request](https://gitlab.com/digital-research-academy/dra-website/-/merge_requests) to feed your changes back into the website.

If you run into any issues or have questions, please ask [Heidi](https://heidiseibold.com/contact/).


### Trainer profiles

Trainer profiles are generated automatically using the code in `_trainers_automation/`.

To add or edit your profile, it is easiest to fill in or edit the trainer form (google form), we sent you.
Afterwards, please ask Heidi to render your profile.

