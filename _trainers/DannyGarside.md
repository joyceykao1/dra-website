---
title: Danny Garside
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/DannyGarside.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/DannyGarside.jpg
     image_alt: Danny
---

### Topics:

Open Science, Data Literacy, Research Software Engineering, Team
management

### Background:

Being an open science advocate during neuroscience PhD and postdoc.
Infrastructure co-lead on Turing Way. Open Life Sciences graduate.

### Training style:

interactive workshops

### Additional info:

Swimming and growing rhubarb

City: London (?) <br/> Country: UK <br/> ☑ Happy to offer services
online <br/> ➢ I am happy to travel (Ground travel preferred)
